use std::env;

#[derive(Debug)]

pub struct Aa {
    u: u8,
    c: i128,
}

fn arsac_loop(mut s: Vec<Aa>, mut v: i128) -> i128 {
    loop {
        //println!("arsac({:?}, {})", s, v);
        let uc = s.pop();
        match (uc, v) {
            (None, _) => {
                return v;
            }
            (_, -1) => {
                v = 1;
                // arsac(s, 1)
            }
            (Some(Aa { u: 0, c }), _) => {
                v += c;
                // arsac(s, v)
            }
            (Some(Aa { u, c }), _) => {
                if c > 1 {
                    s.push(Aa { u, c: c - 1 });
                }

                if u > 1 {
                    if v != 0 {
                        s.push(Aa { u: u - 1, c: v });
                    }
                    s.extend((1..u - 1).rev().map(|u| Aa { u, c: 1 }));
                    s.push(Aa { u: 0, c: 2 });
                } else {
                    s.push(Aa { u: 0, c: v + 1 })
                }
                v = 1;
                // arsac(s, 1)
            }
        }
    }
}

fn arsac(mut s: Vec<Aa>, v: i128) -> i128 {
    let uc = s.pop();

    //println!("arsac({:?}, {})", s, v);

    match (uc, v) {
        (None, _) => v,
        (_, -1) => arsac(s, 1),
        (Some(Aa { u: 0, c }), _) => arsac(s, v + c),
        (Some(Aa { u, c }), _) => {
            if c > 1 {
                s.push(Aa { u, c: c - 1 });
            }

            if u > 1 {
                if v != 0 {
                    s.push(Aa { u: u - 1, c: v });
                }
                s.extend((1..u - 1).rev().map(|u| Aa { u, c: 1 }));
                s.push(Aa { u: 0, c: 2 });
            } else {
                s.push(Aa { u: 0, c: v + 1 })
            }
            arsac(s, 1)
        }
    }
}

fn ackermann_arsac_loop(m: u8, n: i128) -> i128 {
    let mut s: Vec<Aa> = Vec::with_capacity(usize::from(m));
    s.push(Aa { u: m, c: 1 });
    arsac_loop(s, n)
}

fn ackermann_arsac(m: u8, n: i128) -> i128 {
    let mut s: Vec<Aa> = Vec::with_capacity(usize::from(m));
    s.push(Aa { u: m, c: 1 });
    arsac(s, n)
}

fn ackermann(m: u8, n: i128) -> i128 {
    match (m, n) {
        (0, _) => n + 1,
        (_, 0) => ackermann(m - 1, 1),
        _ => ackermann(m - 1, ackermann(m, n - 1)),
    }
}

fn rec_loop(n: i128, res: i128) -> i128 {
    match n {
        0 => res,
        _ => {
            if res % n == 0 {
                rec_loop(n - 1, res + 2)
            } else {
                rec_loop(n - 1, res + 1)
            }
        }
    }
}

fn ma_loop(n: i128) -> i128 {
    rec_loop(n, 0)
}

fn main() {
    let mut a = env::args();
    a.next();
    let (m, n) = (
        a.next().unwrap().parse().unwrap(),
        a.next().unwrap().parse().unwrap(),
    );
    println!("Hello, world!");
    println!("ma_loop({}) = {}", n, ma_loop(n));
    println!(
        "ackermann_arsac_loop({},{}) = {}",
        m,
        n,
        ackermann_arsac_loop(m, n)
    );
    println!(
        "ackermann_arsac     ({},{}) = {}",
        m,
        n,
        ackermann_arsac(m, n)
    );
    println!("ackermann           ({},{}) = {}", m, n, ackermann(m, n));
}
